This is the guideline to maintain project.

Here are the tips to avoid unwanted git related issues:

# Commit Related Changes
# Commit often
# Don’t Commit Half-Done Work
# Test Before You Commit
# Write Good Commit Messages
# Version Control is not a Backup System
# Use Branches
